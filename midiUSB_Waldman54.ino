int PinRow[] = {10, 11, 12, 13, 14, 15, 16}; //Rows
int pinCol[]  = {2, 3, 4, 5, 6, 7, 8, 9}; //Collums
int joyY = 17;
int lastJoyYval = 0;

int key = 0; 
int oct = 0;

int bState = 1;
int bArray[7][8];
int lState[7][8];

void MidiMessage(byte b1, byte b2, byte b3)
{
  Serial.write(b1);
  Serial.write(b2);
  Serial.write(b3);
}

void setup()
{
  for (int nR = 0; nR <= 6; nR++) 
  {
     pinMode(PinRow[nR], OUTPUT);
     digitalWrite(PinRow[nR], HIGH);
  }

  for (int nC = 0; nC <= 7; nC++) 
  {
     pinMode(pinCol[nC], INPUT_PULLUP);
  } 
   
  Serial.begin(115200);
}
 
void loop()
{
    //scans all lines, turning off one at a time
    for (int nR = 0; nR <= 6; nR++)
    {
      digitalWrite(PinRow[nR], LOW);
      
      //scans all columns checking if any button is pressed
      for (int nC = 0; nC <= 7; nC++) 
      {
        
        int bState = digitalRead(pinCol[nC]);
        
        bArray[nR][nC] = bState;

        if (bArray[0][0] == 0)
        {
          MidiMessage(144, 48, 127);
        }
        if (bArray[0][1] == 0)
        {
          MidiMessage(144, 49, 127);
        }
        if (bArray[0][2] == 0)
        {
          MidiMessage(144, 50, 127);
        }
        if (bArray[0][3] == 0)
        {
          MidiMessage(144, 51, 127);
        }
        if (bArray[0][4] == 0)
        {
          MidiMessage(144, 52, 127);
        }
        if (bArray[0][5] == 0)
        {
          MidiMessage(144, 53, 127);
        }
        if (bArray[0][6] == 0)
        {
          MidiMessage(144, 54, 127);
        }
        if (bArray[0][7] == 0)
        {
          MidiMessage(144, 55, 127);
        }
        if (bArray[1][0] == 0)
        {
          MidiMessage(144, 56, 127);
        }
        if (bArray[1][1] == 0)
        {
          MidiMessage(144, 57, 127);
        }
        if (bArray[1][2] == 0)
        {
          MidiMessage(144, 58, 127);
        }
        if (bArray[1][3] == 0)
        {
          MidiMessage(144, 59, 127);
        }
        if (bArray[1][4] == 0)
        {
          MidiMessage(144, 60, 127);
        }
        if (bArray[1][5] == 0)
        {
          MidiMessage(144, 61, 127);
        }
        if (bArray[1][6] == 0)
        {
          MidiMessage(144, 62, 127);
        }
        if (bArray[1][7] == 0)
        {
          MidiMessage(144, 63, 127);
        }
        if (bArray[2][0] == 0)
        {
          MidiMessage(144, 64, 127);
        }
        if (bArray[2][1] == 0)
        {
          MidiMessage(144, 65, 127);
        }
        if (bArray[2][2] == 0)
        {
          MidiMessage(144, 66, 127);
        }
        if (bArray[2][3] == 0)
        {
          MidiMessage(144, 67, 127);
        }
        if (bArray[2][4] == 0)
        {
          MidiMessage(144, 68, 127);
        }
        if (bArray[2][5] == 0)
        {
          MidiMessage(144, 69, 127);
        }
        if (bArray[2][6] == 0)
        {
          MidiMessage(144, 70, 127);
        }
        if (bArray[2][7] == 0)
        {
          MidiMessage(144, 71, 127);
        }
        if (bArray[3][0] == 0)
        {
          MidiMessage(144, 72, 127);
        }
        if (bArray[3][1] == 0)
        {
          MidiMessage(144, 73, 127);
        }
        if (bArray[3][2] == 0)
        {
          MidiMessage(144, 74, 127);
        }
        if (bArray[3][3] == 0)
        {
          MidiMessage(144, 75, 127);
        }
        if (bArray[3][4] == 0)
        {
          MidiMessage(144, 76, 127);
        }
        if (bArray[3][5] == 0)
        {
          MidiMessage(144, 77, 127);
        }
        if (bArray[3][6] == 0)
        {
          MidiMessage(144, 78, 127);
        }
        if (bArray[3][7] == 0)
        {
          MidiMessage(144, 79, 127);
        }
        if (bArray[4][0] == 0)
        {
          MidiMessage(144, 80, 127);
        }
        if (bArray[4][1] == 0)
        {
          MidiMessage(144, 81, 127);
        }
        if (bArray[4][2] == 0)
        {
          MidiMessage(144, 82, 127);
        }
        if (bArray[4][3] == 0)
        {
          MidiMessage(144, 83, 127);
        }
        if (bArray[4][4] == 0)
        {
          MidiMessage(144, 84, 127);
        }
        if (bArray[4][5] == 0)
        {
          MidiMessage(144, 85, 127);
        }
        if (bArray[4][6] == 0)
        {
          MidiMessage(144, 86, 127);
        }
        if (bArray[4][7] == 0)
        {
          MidiMessage(144, 87, 127);
        }
        if (bArray[5][0] == 0)
        {
          MidiMessage(144, 88, 127);
        }
        if (bArray[5][1] == 0)
        {
          MidiMessage(144, 89, 127);
        }
        if (bArray[5][2] == 0)
        {
          MidiMessage(144, 90, 127);
        }
        if (bArray[5][3] == 0)
        {
          MidiMessage(144, 91, 127);
        }
        if (bArray[5][4] == 0)
        {
          MidiMessage(144, 92, 127);
        }
        if (bArray[5][5] == 0)
        {
          MidiMessage(144, 93, 127);
        }
        if (bArray[5][6] == 0)
        {
          MidiMessage(144, 94, 127);
        }
        if (bArray[5][7] == 0)
        {
          MidiMessage(144, 95, 127);
        }
        if (bArray[6][0] == 0)
        {
          MidiMessage(144, 96, 127);
        }
        if (bArray[6][1] == 0)
        {
          MidiMessage(144, 97, 127);
        }
        if (bArray[6][2] == 0)
        {
          MidiMessage(144, 98, 127);
        }
        if (bArray[6][3] == 0)
        {
          MidiMessage(144, 99, 127);
        }
        if (bArray[6][4] == 0)
        {
          MidiMessage(144, 100, 127);
        }
        if (bArray[6][5] == 0)
        {
          MidiMessage(144, 101, 127);
        }
        if (bArray[6][6] == 0)
        {
          MidiMessage(144, 102, 127);
        }
        if (bArray[6][7] == 0)
        {
          MidiMessage(144, 103, 127);
        }

               
        //disables the key when it is released
        if (bArray[0][0] != lState[0][0])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 48, 0);
          }
        }
        if (bArray[0][1] != lState[0][1])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 49, 0);
          }
        }
        if (bArray[0][2] != lState[0][2])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 50, 0);
          }
        }
        if (bArray[0][3] != lState[0][3])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 51, 0);
          }
        }
        if (bArray[0][4] != lState[0][4])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 52, 0);
          }
        }
        if (bArray[0][5] != lState[0][5])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 53, 0);
          }
        }
        if (bArray[0][6] != lState[0][6])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 54, 0);
          }
        }
        if (bArray[0][7] != lState[0][7])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 55, 0);
          }
        }
        if (bArray[1][0] != lState[1][0])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 56, 0);
          }
        }
        if (bArray[1][1] != lState[1][1])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 57, 0);
          }
        }
        if (bArray[1][2] != lState[1][2])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 58, 0);
          }
        }
        if (bArray[1][3] != lState[1][3])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 59, 0);
          }
        }
        if (bArray[1][4] != lState[1][4])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 60, 0);
          }
        }
        if (bArray[1][5] != lState[1][5])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 61, 0);
          }
        }
        if (bArray[1][6] != lState[1][6])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 62, 0);
          }
        }
        if (bArray[1][7] != lState[1][7])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 63, 0);
          }
        }
        if (bArray[2][0] != lState[2][0])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 64, 0);
          }
        }
        if (bArray[2][1] != lState[2][1])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 65, 0);
          }
        }
        if (bArray[2][2] != lState[2][2])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 66, 0);
          }
        }
        if (bArray[2][3] != lState[2][3])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 67, 0);
          }
        }
        if (bArray[2][4] != lState[2][4])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 68, 0);
          }
        }
        if (bArray[2][5] != lState[2][5])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 69, 0);
          }
        }
        if (bArray[2][6] != lState[2][6])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 70, 0);
          }
        }
        if (bArray[2][7] != lState[2][7])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 71, 0);
          }
        }
        if (bArray[3][0] != lState[3][0])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 72, 0);
          }
        }
        if (bArray[3][1] != lState[3][1])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 73, 0);
          }
        }
        if (bArray[3][2] != lState[3][2])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 74, 0);
          }
        }
        if (bArray[3][3] != lState[3][3])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 75, 0);
          }
        }
        if (bArray[3][4] != lState[3][4])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 76, 0);
          }
        }
        if (bArray[3][5] != lState[3][5])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 77, 0);
          }
        }
        if (bArray[3][6] != lState[3][6])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 78, 0);
          }
        }
        if (bArray[3][7] != lState[3][7])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 79, 0);
          }
        }
        if (bArray[4][0] != lState[4][0])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 80, 0);
          }
        }
        if (bArray[4][1] != lState[4][1])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 81, 0);
          }
        }
        if (bArray[4][2] != lState[4][2])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 82, 0);
          }
        }
        if (bArray[4][3] != lState[4][3])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 83, 0);
          }
        }
        if (bArray[4][4] != lState[4][4])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 84, 0);
          }
        }
        if (bArray[4][5] != lState[4][5])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 85, 0);
          }
        }
        if (bArray[4][6] != lState[4][6])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 86, 0);
          }
        }
        if (bArray[4][7] != lState[4][7])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 87, 0);
          }
        }
        if (bArray[5][0] != lState[5][0])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 88, 0);
          }
        }
        if (bArray[5][1] != lState[5][1])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 89, 0);
          }
        }
        if (bArray[5][2] != lState[5][2])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 90, 0);
          }
        }
        if (bArray[5][3] != lState[5][3])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 91, 0);
          }
        }
        if (bArray[5][4] != lState[5][4])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 92, 0);
          }
        }
        if (bArray[5][5] != lState[5][5])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 93, 0);
          }
        }
        if (bArray[5][6] != lState[5][6])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 94, 0);
          }
        }
        if (bArray[5][7] != lState[5][7])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 95, 0);
          }
        }
        if (bArray[6][0] != lState[6][0])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 96, 0);
          }
        }
        if (bArray[6][1] != lState[6][1])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 97, 0);
          }
        }
        if (bArray[6][2] != lState[6][2])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 98, 0);
          }
        }
        if (bArray[6][3] != lState[6][3])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 99, 0);
          }
        }
        if (bArray[6][4] != lState[6][4])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 100, 0);
          }
        }
        if (bArray[6][5] != lState[6][5])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 101, 0);
          }
        }
        if (bArray[6][6] != lState[6][6])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 102, 0);
          }
        }
        if (bArray[6][7] != lState[6][7])
        {
          if (bState == HIGH)
          {
            MidiMessage(144, 103, 0);
          }
        }

        
        lState[nR][nC] = bArray[nR][nC];

        //delay(5);
      }
      
      digitalWrite(PinRow[nR], HIGH);
    }

      //joystick readout for pitch bending effect
      int diff = 8;
      int joyYval = analogRead(joyY);
      int joyYvalDiff = joyYval - lastJoyYval;
  
      if (abs(joyYvalDiff) > diff)
      {
        // construct 14 bit modulation value and send over midi
        int modulation = map(joyYval, 0, 1023, -8000, 8000);
        unsigned int change = 0x2000 + modulation;
        unsigned char low = change & 0x7F;
        unsigned char high = (change >> 7) & 0x7F;
  
        MidiMessage(224, low, high);
  
        lastJoyYval = joyYval;
      }           
}
