<h1>Turn the Waldman KeyPro 54 keyboard into a midi controller</h1>
<p>
In this example I show how to transform the Waldman KeyPro 54 keyboard into a midi controller.<br>
Internally it has a button matrix of 7 rows and 8 columns.<br>
<table class="tg">
<thead>
  <tr>
    <th class="tg-baqh">R1</th>
    <th class="tg-baqh">C</th>
    <th class="tg-baqh">G</th>
    <th class="tg-baqh">D</th>
    <th class="tg-baqh">A</th>
    <th class="tg-baqh">E</th>
    <th class="tg-baqh">B<br></th>
    <th class="tg-baqh">F#/Gb</th>
    <th class="tg-baqh">C#/Db</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-baqh">R2</td>
    <td class="tg-baqh">C#/Db</td>
    <td class="tg-baqh">G#/Ab</td>
    <td class="tg-baqh">D#/Eb<br></td>
    <td class="tg-baqh">A#/Bb</td>
    <td class="tg-baqh">F</td>
    <td class="tg-baqh">C</td>
    <td class="tg-baqh">G</td>
    <td class="tg-baqh">D</td>
  </tr>
  <tr>
    <td class="tg-baqh">R3</td>
    <td class="tg-baqh">D</td>
    <td class="tg-baqh">A</td>
    <td class="tg-baqh">E</td>
    <td class="tg-baqh">B</td>
    <td class="tg-baqh">F#/Gb</td>
    <td class="tg-baqh">C#/Db</td>
    <td class="tg-baqh">G#/Ab</td>
    <td class="tg-baqh">D#/Eb</td>
  </tr>
  <tr>
    <td class="tg-baqh">R4</td>
    <td class="tg-baqh">D#/Eb</td>
    <td class="tg-baqh">A#/Bb<br></td>
    <td class="tg-baqh">F</td>
    <td class="tg-baqh">C</td>
    <td class="tg-baqh">G</td>
    <td class="tg-baqh">D</td>
    <td class="tg-baqh">A</td>
    <td class="tg-baqh">E</td>
  </tr>
  <tr>
    <td class="tg-baqh">R5</td>
    <td class="tg-baqh">E</td>
    <td class="tg-baqh">B</td>
    <td class="tg-baqh">F#/Gb</td>
    <td class="tg-baqh">C#/Db</td>
    <td class="tg-baqh">G#/Ab</td>
    <td class="tg-baqh">D#/Eb</td>
    <td class="tg-baqh">A#/Bb</td>
    <td class="tg-baqh">F</td>
  </tr>
  <tr>
    <td class="tg-baqh">R6</td>
    <td class="tg-baqh">F</td>
    <td class="tg-baqh">C</td>
    <td class="tg-baqh">G</td>
    <td class="tg-baqh">D</td>
    <td class="tg-baqh">A</td>
    <td class="tg-baqh">E</td>
    <td class="tg-baqh">B</td>
    <td class="tg-baqh">X</td>
  </tr>
  <tr>
    <td class="tg-baqh">R7</td>
    <td class="tg-baqh">F#/Gb</td>
    <td class="tg-baqh">C#/Db</td>
    <td class="tg-baqh">G#/Ab</td>
    <td class="tg-baqh">D#/Eb</td>
    <td class="tg-baqh">A#/Bb</td>
    <td class="tg-baqh">F</td>
    <td class="tg-baqh">C</td>
    <td class="tg-baqh">X</td>
  </tr>
  <tr>
    <td class="tg-baqh"></td>
    <td class="tg-baqh">C1</td>
    <td class="tg-baqh">C2</td>
    <td class="tg-baqh">C3</td>
    <td class="tg-baqh">C4</td>
    <td class="tg-baqh">C5</td>
    <td class="tg-baqh">C6</td>
    <td class="tg-baqh">C7</td>
    <td class="tg-baqh">C8</td>
  </tr>
</tbody>
</table>
<p>
Using the scan method, each line is energized (Low Level to INPUT_PULLUP) and then it is checked whether the signal appears in any column if the button is pressed.<br>
For rows we have pins 10-16 and for columns 2-9.<br>
Here in my OS I am using the ttymidi program to convert the 3 bytes coming from the Arduino to midi commands, so amsynth receives them and converts them into sound through QjackCtl.<br>
By default, ttymidi uses 115200 as baudrate.<br>
<p>
<h4>Command Line</h4>
<p>
ttymidi -s /dev/ttyUSB0 -v<br>

